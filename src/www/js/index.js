'use strict'

const my_shared_code = require('../js/my_shared_code')

document.addEventListener('DOMContentLoaded', function() {
    const message_pane = document.getElementById('message-pane')
    my_shared_code.writeContent(message_pane)
})
